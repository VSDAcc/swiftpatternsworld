//: [Previous](@previous)

import Foundation
import UIKit

//MARK:-Proxy
protocol FootballPlayer {
    var name: String { get }
    func move() -> String
    func getName() -> String
}
class Ronaldo: FootballPlayer {
    
    var name: String = "Ronaldo"
    
    func move() -> String {
        return "Ronaldo Move"
    }
    
    func getName() -> String {
        return name
    }
}
class ProxyOfFootballPlayer: FootballPlayer {
    
    private var owner: FootballPlayer
    var name: String {
        get {
            return owner.name
        }
    }
    
    init(owner: FootballPlayer) {
        self.owner = owner
    }
    
    func getName() -> String {
        return owner.getName()
    }
    
    func move() -> String {
        return owner.move()
    }
}



protocol ImageDownloadService {
    func downloadImage(_ url: URL,
                       onSuccess: @escaping(_ data: Data) -> (),
                       onFailure: @escaping(_ error: String) -> ())
}
class ImageDataService: ImageDownloadService {
    
    func downloadImage(_ url: URL, onSuccess: @escaping (Data) -> (), onFailure: @escaping (String) -> ()) {
        let config = URLSessionConfiguration.default
        config.requestCachePolicy = .reloadIgnoringLocalAndRemoteCacheData
        config.urlCache = nil
        
        let session = URLSession(configuration: config)
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            onSuccess(data!)
        }
        dataTask.resume()
    }
}
class ProxyImageDataService: ImageDownloadService {
    
    private var imageCache: Data? = nil
    private var dataService: ImageDownloadService
    
    init(service: ImageDownloadService) {
        self.dataService = service
    }
    
    func downloadImage(_ url: URL, onSuccess: @escaping (Data) -> (), onFailure: @escaping (String) -> ()) {
        if imageCache == nil {
            dataService.downloadImage(url, onSuccess: { [weak self] (data) in
                self?.imageCache = data
                onSuccess(data)
            }) { (error) in
                print(error)
            }
        } else {
            onSuccess(imageCache!)
        }
    }
}
