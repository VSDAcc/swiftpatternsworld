
import Foundation

//MARK:-FactoryMethod
protocol Car {
    func drive()
}
class Sedan: Car {
    func drive() {
        print("sedan driving")
    }
}
class SUV: Car {
    func drive() {
        print("suv driving")
    }
}

protocol CarFactoryMethod {
    func produceCar() -> Car
}
class SedanFactory: CarFactoryMethod {
    func produceCar() -> Car {
        return Sedan()
    }
}
class SuvFactory: CarFactoryMethod {
    func produceCar() -> Car {
        return SUV()
    }
}
final class CarFactory: CarFactoryMethod {
    
    fileprivate var factory: CarFactoryMethod
    
    init(factory: CarFactoryMethod) {
        self.factory = factory
    }
    
    func setFactory(_ factory: CarFactoryMethod) {
        self.factory = factory
    }
    
    func produceCar() -> Car {
        return factory.produceCar()
    }
}
let sedanFactory = SedanFactory()
let suvFactory = SuvFactory()
let carFactory = CarFactory(factory: suvFactory)
let suv = carFactory.produceCar()
suv.drive()
