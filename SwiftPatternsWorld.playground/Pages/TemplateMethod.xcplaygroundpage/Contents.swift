
import Foundation
import UIKit
//MARK:-TemplateMethod

protocol ButtonTemplate {
    func setColor()
    func setTitle()
    func setColorTitle()
}
class AuthenticationButton: UIButton, ButtonTemplate {
    
    //MARK:-Loading
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialize() {
        setColor()
        setTitle()
        setColorTitle()
    }
        
    func setColor() {
        self.backgroundColor = .black
    }
    
    func setTitle() {
        self.setTitle("title", for: .normal)
    }
    
    func setColorTitle() {
        self.setTitleColor(.white, for: .normal)
    }
}
class LoginButton: AuthenticationButton {
    
    override func setTitle() {
        self.setTitle("Login", for: .normal)
    }
}
class PasswordButton: AuthenticationButton {
    
    override func setColor() {
        self.backgroundColor = .blue
    }
    
    override func setTitle() {
        self.setTitle("Password", for: .normal)
    }
    
    override func setColorTitle() {
        self.setTitleColor(.yellow, for: .normal)
    }
}

