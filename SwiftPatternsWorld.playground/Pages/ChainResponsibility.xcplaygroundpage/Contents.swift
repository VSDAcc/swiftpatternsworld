
import Foundation

//MAKR:-Chain Responsibility
protocol BasicItem {
    var name: String { get set }
}
class Toy: BasicItem {
    
    var name: String
    
    init(name: String) {
        self.name = name
    }
}
class Electronic: BasicItem {
    
    var name: String
    
    init(name: String) {
        self.name = name
    }
}
class Box: BasicItem {
    
    var name: String
    
    init(name: String) {
        self.name = name
    }
}

protocol Chain {
    typealias Completion = (_ newItem: BasicItem?) -> ()
    var next: Chain? { get set }
    func handle(_ item: BasicItem, onSuccess: Completion)
}
class ToyHandler: Chain {
    
    var next: Chain?
    
    required init(chain: Chain? = nil) {
        self.next = chain
    }
    
    func handle(_ item: BasicItem, onSuccess: Completion) {
        if item is Toy {
            onSuccess(Toy(name: "Teddy"))
        } else {
            print("This is not an \(item.name), looking for more")
            next?.handle(item, onSuccess: onSuccess)
        }
    }
}
class BoxHandler: Chain {
    
    var next: Chain?
    
    required init(chain: Chain? = nil) {
        self.next = chain
    }
    
    func handle(_ item: BasicItem, onSuccess: Completion) {
        if item is Box {
            onSuccess(Box(name: "Box"))
        } else {
            print("This is not an \(item.name), looking for more")
            next?.handle(item, onSuccess: onSuccess)
        }
    }
}
class ElectronicHandler: Chain {
    
    var next: Chain?
    
    required init(chain: Chain? = nil) {
        self.next = chain
    }
    
    func handle(_ item: BasicItem, onSuccess: Completion) {
        if item is Electronic {
            onSuccess(Electronic(name: "Electro power"))
        } else {
            print("This is not an \(item.name), looking for more")
            next?.handle(item, onSuccess: onSuccess)
        }
    }
}
protocol ChainClientManager {
    var next: Chain? { get set }
    var item: BasicItem { get set }
    func handle()
}
class ChainClient: ChainClientManager {
    
    var next: Chain?
    var item: BasicItem
    
    init(item: BasicItem, chain: Chain? = nil) {
        self.item = item
        self.next = chain
    }
    
    func handle() {
        guard let chain = next else { return
            print("chain is not found")
        }
        chain.handle(item) { (model) in
            if let newModel = model {
                print(newModel.name)
            } else {
                print("chain is not resolve any item")
            }
        }
    }
}
class ChainBuilderDirector: ChainBuilder { }
protocol ChainBuilder {
    func resolve(with item: BasicItem) -> ChainClientManager
    func resolveElectronic(with chain: Chain?) -> ElectronicHandler
    func resolveBox(with chain: Chain?) -> BoxHandler
    func resolveToy(with chain: Chain?) -> ToyHandler
}
extension ChainBuilder {
    func resolve(with item: BasicItem) -> ChainClientManager {
        return ChainClient(item: item, chain: resolveElectronic(with: resolveBox(with: resolveToy(with: nil))))
    }
    
    func resolveElectronic(with chain: Chain?) -> ElectronicHandler {
        return ElectronicHandler(chain: chain)
    }
    
    func resolveBox(with chain: Chain?) -> BoxHandler {
        return BoxHandler(chain: chain)
    }
    
    func resolveToy(with chain: Chain?) -> ToyHandler {
        return ToyHandler(chain: chain)
    }
}
let chain = ChainBuilderDirector()
let client = chain.resolve(with: Box(name: "hello world"))
client.handle()
