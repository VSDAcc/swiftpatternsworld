
import Foundation
import UIKit
//MARK:-Facade

protocol DiscountDepartment {
    func getDiscount(forPizza count: Int) -> Double
}
class DiscountPizzaDepartment: DiscountDepartment {
    
    func getDiscount(forPizza count: Int) -> Double {
        switch count {
        case 1: return 0.0
        case 2..<5: return 10.0
        case 5..<10: return 20.0
        default: return 20.0
        }
    }
}
protocol OrderDepartment {
    var price: Double { get set }
    func getPrice(forPizza count: Int) -> Double
}
class OrderPizzaDepartment: OrderDepartment {
    
    var price: Double = 50.0
    
    func getPrice(forPizza count: Int) -> Double {
        return Double(count) * price
    }
}
protocol DeliveryDepartment {
    func getDeliveryTime(city name: String) -> Double
}
class DeliveryPizzaDepartment: DeliveryDepartment {
    
    func getDeliveryTime(city name: String) -> Double {
        switch name {
        case "Kiev": return 50.0
        default: return 120.0
        }
    }
}
protocol PizzaHeaderDepartment {
    var discount: DiscountDepartment { get set }
    var order: OrderDepartment { get set }
    var deliveryTime: DeliveryDepartment { get set }
}
class PizzaRestourant: PizzaHeaderDepartment {
    
    var discount: DiscountDepartment
    var order: OrderDepartment
    var deliveryTime: DeliveryDepartment
    
    init(discount: DiscountDepartment, order: OrderDepartment, deliveryTime: DeliveryDepartment) {
        self.discount = discount
        self.order = order
        self.deliveryTime = deliveryTime
    }
}
class PizzaRestourantOrderInformation {
    
    private var pizzaHeaderDepartment: PizzaHeaderDepartment
    
    required init (headerDepartment: PizzaHeaderDepartment) {
        self.pizzaHeaderDepartment = headerDepartment
    }
    
    func getOrderInfo(count: Int, adress: String) -> (pizzaPrice: Double, delivaryTime: Double) {
        let regularPrice = pizzaHeaderDepartment.order.getPrice(forPizza: count)
        let discountPrice = pizzaHeaderDepartment.discount.getDiscount(forPizza: count)
        let currentPrice = regularPrice - regularPrice * discountPrice / 100
        let currentDeliveryTime = pizzaHeaderDepartment.deliveryTime.getDeliveryTime(city: adress)
        return (currentPrice, currentDeliveryTime)
    }
}

enum ErrorType: Error {
    case destinationPathError
    case jpegDataError
    case pngDataError
}
protocol ImageDataStrategy {
    func data(from image: UIImage) throws -> Data
}
class PngData: ImageDataStrategy {
    
    func data(from image: UIImage) throws -> Data {
        guard let pngData = image.pngData() else {
            throw ErrorType.pngDataError
        }
        return pngData
    }
}
class JpegData: ImageDataStrategy {
    
    var quality: CGFloat = 1000.0
    
    func data(from image: UIImage) throws -> Data {
        guard let jpegData = image.jpegData(compressionQuality: quality) else {
            throw ErrorType.jpegDataError
        }
        return jpegData
    }
}
class ImageDataComressor: ImageDataStrategy {
    
    var compressor: ImageDataStrategy
    
    init(compressor: ImageDataStrategy) {
        self.compressor = compressor
    }
    
    func data(from image: UIImage) throws -> Data {
        return try compressor.data(from: image)
    }
}
class ImageDataProvider {
    
    private var dataCompressor = ImageDataComressor(compressor: PngData())
    
    public func data(from image: UIImage, type: ImageDataStrategy) throws -> Data {
        dataCompressor.compressor = type
        return try dataCompressor.data(from: image)
    }
}
class ImageSaverFacade {
    
    enum ImageType {
        case jpeg(compression: CGFloat)
        case png
    }
    
    private var imageDataProvider = ImageDataProvider()
    
    func save(image: UIImage, type: ImageType) throws {
        let data = try pickDataType(image: image, type: type)
        print("Data saved\(String(describing: data))")
    }
    
    private func pickDataType(image: UIImage, type: ImageType) throws -> Data {
        switch type {
        case .jpeg(let compression):
            let jpegData = JpegData()
            jpegData.quality = compression
            return try imageDataProvider.data(from: image, type: jpegData)
        case .png:
            let pngData = PngData()
            return try imageDataProvider.data(from: image, type: pngData)
        }
    }
}
let facade = ImageSaverFacade()
do {
    try facade.save(image: UIImage(), type: .png)
} catch {
    print(error.localizedDescription)
}

