//: [Previous](@previous)

import Foundation
import UIKit

//MARK:-Decorator
protocol Coffe {
    func getCost() -> Double
    func getIngridients() -> String
}
class SimpleCoffe: Coffe {
    
    func getCost() -> Double {
        return 1.0
    }
    
    func getIngridients() -> String {
        return "American coffe"
    }
}
protocol CoffeDecorator: Coffe {
    var decoratedCoffe: Coffe { get }
}
class MilkIngridient: CoffeDecorator {
    
    private var ingridientName: String = "Milk"
    private var ingridientsSeparator = ","
    private var ingridientCost: Double = 5
    let decoratedCoffe: Coffe
    
    init(coffe: Coffe, cost: Double) {
        self.decoratedCoffe = coffe
        self.ingridientCost = cost
    }
    
    func getIngridients() -> String {
        return decoratedCoffe.getIngridients() + ingridientsSeparator + ingridientName
    }
    
    func getCost() -> Double {
        return decoratedCoffe.getCost() + ingridientCost
    }
}
class ChocolateIngridient: CoffeDecorator {
    
    private var ingridientName: String = "Chocolate"
    private var ingridientsSeparator = ","
    private var ingridientCost: Double = 10
    let decoratedCoffe: Coffe
    
    init(coffe: Coffe, cost: Double ) {
        self.decoratedCoffe = coffe
        self.ingridientCost = cost
    }
    
    func getIngridients() -> String {
        return decoratedCoffe.getIngridients() + ingridientsSeparator + ingridientName
    }
    
    func getCost() -> Double {
        return decoratedCoffe.getCost() + ingridientCost
    }
}

let simpleCoffe = SimpleCoffe()
let milkCoffe = MilkIngridient(coffe: simpleCoffe, cost: 5.0)
milkCoffe.getCost()
milkCoffe.getIngridients()
let milkAndChocolateCoffe = ChocolateIngridient(coffe: milkCoffe, cost: 10.0)
milkAndChocolateCoffe.getCost()
milkAndChocolateCoffe.getIngridients()


protocol Mercedes {
    func getPrice() -> Double
    func getModel() -> String
}
class MercedesCLA: Mercedes {
    
    private var price: Double = 1000.0
    private var model: String = "CLA"
    
    func getModel() -> String {
        return model
    }
    
    func getPrice() -> Double {
        return price
    }
}
class BaseMercedesDecorator: Mercedes {
    
    private let mercedes: Mercedes
    
    init(mercedes: Mercedes) {
        self.mercedes = mercedes
    }
    
    func getPrice() -> Double {
        return mercedes.getPrice()
    }
    
    func getModel() -> String {
        return mercedes.getModel()
    }
}
class MercedesGoldCLA: BaseMercedesDecorator {
    
    fileprivate var model: String = "Gold"
    fileprivate var upgradePrice: Double = 400.0
    
    init(mercedes: Mercedes, upgradePrice: Double) {
        super.init(mercedes: mercedes)
        self.upgradePrice = upgradePrice
    }
    
    override func getPrice() -> Double {
        return super.getPrice() + upgradePrice
    }
    
    override func getModel() -> String {
        return super.getModel() + model
    }
}
class MercedesPlatinumCLA: BaseMercedesDecorator {
    
    fileprivate var model: String = "Platinum"
    fileprivate var upgradePrice: Double = 800.0
    
    override func getPrice() -> Double {
        return super.getPrice() + upgradePrice
    }
    
    override func getModel() -> String {
        return super.getModel() + model
    }
}
var mercedes: Mercedes = MercedesCLA()
mercedes = MercedesGoldCLA(mercedes: mercedes, upgradePrice: 200.0)
mercedes.getModel()
mercedes.getPrice()
mercedes = MercedesPlatinumCLA(mercedes: mercedes)
mercedes.getModel()
mercedes.getPrice()
