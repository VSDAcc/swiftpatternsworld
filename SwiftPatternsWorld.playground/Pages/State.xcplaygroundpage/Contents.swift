//: [Previous](@previous)

import Foundation
//MARK:-State

protocol FileContext: class {
    func setState(_ state: FileState)
    func showInformation()
    func uploadFile()
    func startLoading()
    func finishLoading()
    func canceled()
    func getInformationState() -> InformationFileState
    func getUploadedState() -> UploadedFileState
    func getLoadedState() -> LoadedFileState
    func getFinishedState() -> FinishedFileState
    func getCanceledState() -> CanceledFileState
}
class ProfileFile: FileContext {
    
    fileprivate var state: FileState!
    
    init() {
        self.state = InformationFileState(file: self)
    }
    
    func setState(_ state: FileState) {
        self.state = state
    }
    
    func showInformation() {
        state.informationScreen()
    }
    
    func uploadFile() {
        state.upload()
    }
    
    func startLoading() {
        state.load()
    }
    
    func finishLoading() {
        state.finish()
    }
    
    func canceled() {
        state.cancel()
    }
    
    func getInformationState() -> InformationFileState {
        return InformationFileState(file: self)
    }
    
    func getUploadedState() -> UploadedFileState {
        return UploadedFileState(file: self)
    }
    
    func getLoadedState() -> LoadedFileState {
        return LoadedFileState(file: self)
    }
    
    func getFinishedState() -> FinishedFileState {
        return FinishedFileState(file: self)
    }
    
    func getCanceledState() -> CanceledFileState {
        return CanceledFileState(file: self)
    }
}
protocol FileState {
    init(file: FileContext)
    func informationScreen()
    func upload()
    func load()
    func finish()
    func cancel()
}
class InformationFileState: FileState {
    
    fileprivate weak var file: FileContext?
    var information: String = "Welcome to the registration form"
    
    required init(file: FileContext) {
        self.file = file
    }
    
    func informationScreen() {
        print("view shows information \(information)")
    }
    
    func upload() {
        print("InformationFileState start uploading file")
        guard let uploadState = file?.getUploadedState() else { return }
        file?.setState(uploadState)
        file?.uploadFile()
        
    }
    
    func load() {
        print("view shows information \(information)")
    }
    
    func finish() {
        print("view shows information \(information)")
    }
    
    func cancel() {
        guard let cancelState = file?.getCanceledState() else { return }
        file?.setState(cancelState)
        cancelState.informationScreen()
    }
}
class UploadedFileState: FileState {
    
    fileprivate weak var file: FileContext?
    var isUploaded: Bool = Bool.random()
    var picture: String = ""
    
    required init(file: FileContext) {
        self.file = file
    }
    
    func informationScreen() {
        guard let informState = file?.getInformationState() else { return }
        informState.information = "file is upload \(isUploaded)"
        informState.informationScreen()
        file?.setState(informState)
        
    }
    
    func upload() {
        isUploaded = Bool.random()
        if isUploaded {
            picture = "Kitty"
            print("file is successfully uploaded showing load screen")
            guard let loadState = file?.getLoadedState() else { return }
            file?.setState(loadState)
            loadState.picture = picture
            loadState.load()
        } else {
            guard let informState = file?.getInformationState() else { return }
            informState.information = "File is failed to upload"
            informState.informationScreen()
            file?.setState(informState)
        }
    }
    
    func load() {
        print("UploadedFileState Start load")
        if isUploaded {
            guard let loadState = file?.getLoadedState() else { return }
            file?.setState(loadState)
            loadState.picture = picture
            loadState.load()
            
        } else {
            guard let informState = file?.getInformationState() else { return }
            informState.information = "File is failed to upload please try again"
            informState.informationScreen()
            file?.setState(informState)
            
        }
    }
    
    func finish() {
        print("file is not uploaded")
    }
    
    func cancel() {
        guard let cancelState = file?.getCanceledState() else { return }
        file?.setState(cancelState)
        cancelState.upload()
    }
}
class LoadedFileState: FileState {
    
    fileprivate weak var file: FileContext?
    var picture: String = ""
    var progress: Float = 0.0
    
    required init(file: FileContext) {
        self.file = file
    }
    
    func informationScreen() {
        guard let informState = file?.getInformationState() else { return }
        informState.information = "file is loaded with progress \(String(describing: progress)) %"
        informState.informationScreen()
        file?.setState(informState)
        
    }
    
    func upload() {
        guard let informState = file?.getInformationState() else { return }
        informState.information = "Please upload new picture"
        informState.informationScreen()
        file?.setState(informState)
        
    }
    
    func load() {
        progress = 100.0
        print("LoadedFileState start loading with \(progress)")
        if progress == 100.0 {
            guard let finishedState = file?.getFinishedState() else { return }
            file?.setState(finishedState)
            finishedState.finish()
        } else {
            print("File is still loading with progress\(progress)")
        }
    }
    
    func finish() {
        if progress == 100.0 {
            guard let finishedState = file?.getFinishedState() else { return }
            file?.setState(finishedState)
            finishedState.finish()
        } else {
            print("File is still loading with progress\(progress)")
        }
    }
    
    func cancel() {
        guard let cancelState = file?.getCanceledState() else { return }
        file?.setState(cancelState)
        cancelState.load()
    }
}
class FinishedFileState: FileState {
    
    fileprivate weak var file: FileContext?
    
    required init(file: FileContext) {
        self.file = file
    }
    
    func informationScreen() {
        guard let informState = file?.getInformationState() else { return }
        informState.information = "File is successfull loaded"
        informState.informationScreen()
        file?.setState(informState)
        
    }
    
    func upload() {
        print("FinishedFileState is finished")
    }
    
    func load() {
        print("FinishedFileState is finished")
    }
    
    func finish() {
        guard let informState = file?.getInformationState() else { return }
        informState.information = "file is finished and hidinig view view"
        informState.informationScreen()
        file?.setState(informState)
        
    }
    
    func cancel() {
        guard let cancelState = file?.getCanceledState() else { return }
        file?.setState(cancelState)
        cancelState.finish()
    }
    
}
class CanceledFileState: FileState {
    
    fileprivate weak var file: FileContext?
    
    required init(file: FileContext) {
        self.file = file
    }
    
    func informationScreen() {
        if let informState = file?.getInformationState() {
            informState.informationScreen()
            file?.setState(informState)
        }
    }
    
    func upload() {
        if let informState = file?.getInformationState() {
            informState.information = "Upload is filed please try again"
            informState.informationScreen()
            file?.setState(informState)
        }
    }
    
    func load() {
        if let informState = file?.getInformationState() {
            informState.information = "Loading is filed please try again"
            informState.informationScreen()
            file?.setState(informState)
        }
    }
    
    func finish() {
        if let informState = file?.getInformationState() {
            informState.information = "File is finished and hidinig view view"
            informState.informationScreen()
            file?.setState(informState)
        }
    }
    
    func cancel() {
        if let informState = file?.getInformationState() {
            informState.informationScreen()
            file?.setState(informState)
        }
    }
}

let file = ProfileFile()
file.showInformation()
file.uploadFile()


protocol VehicleContextProtocol: class {
    var speed: Int { get set }
    func setState(_ state: VehicleState)
    func accelerate()
    func breakMotor()
    func park()
    func getStoppedState() -> VehicleState
    func getMovingState() -> VehicleState
    func getParkingState() -> VehicleState
}
class VehicleContext: VehicleContextProtocol {
    
    fileprivate var state: VehicleState!
    var speed: Int = 0
    
    init() {
        self.state = StoppedState(vehicle: self)
    }
    
    func setState(_ state: VehicleState) {
        self.state = state
    }
    
    func accelerate() {
        state.accelerate()
    }
    
    func breakMotor() {
        state.breakMotor()
    }
    
    func park() {
        state.park()
    }
    
    func getStoppedState() -> VehicleState {
        return StoppedState(vehicle: self)
    }
    
    func getMovingState() -> VehicleState {
        return MovingState(vehicle: self)
    }
    
    func getParkingState() -> VehicleState {
        return ParkingState(vehicle: self)
    }
}
protocol VehicleState {
    init(vehicle: VehicleContextProtocol)
    func accelerate()
    func breakMotor()
    func park()
}
class StoppedState: VehicleState {
    
    fileprivate weak var vehicle: VehicleContextProtocol?
    
    required init(vehicle: VehicleContextProtocol) {
        self.vehicle = vehicle
    }
    
    func accelerate() {
        self.vehicle?.speed += 5
        if let movingState = self.vehicle?.getMovingState() {
            self.vehicle?.setState(movingState)
            movingState.accelerate()
            print("Start accelerate form Stopped state")
        }
    }
    
    func breakMotor() {
        print("Can't brake... Vehicle is already stopped!")
    }
    
    func park() {
        if let parkingState = self.vehicle?.getParkingState() {
            self.vehicle?.setState(parkingState)
            parkingState.park()
            print("Start parking form Stopped state")
        }
    }
}
class MovingState: VehicleState {
    
    fileprivate weak var vehicle: VehicleContextProtocol?
    
    required init(vehicle: VehicleContextProtocol) {
        self.vehicle = vehicle
    }
    
    func accelerate() {
        self.vehicle?.speed += 5
    }
    
    func breakMotor() {
        self.vehicle?.speed -= 5
        if self.vehicle?.speed == 0, let stoppedState = self.vehicle?.getStoppedState() {
            print("Vehicle braked to a stop")
            self.vehicle?.setState(stoppedState)
        }
    }
    
    func park() {
        print("Can't park the vehicle while it's moving. You need to stop first")
    }
}
class ParkingState: VehicleState {
    
    fileprivate weak var vehicle: VehicleContextProtocol?
    private var parking: Bool = false
    
    required init(vehicle: VehicleContextProtocol) {
        self.vehicle = vehicle
    }
    
    func accelerate() {
        print("Vehicle is automatically parking, you can't accelerate!")
    }
    
    func breakMotor() {
        print("Automatic parking has been aborted")
        stopParking()
    }
    
    func park() {
        guard self.parking == false else {
            print("Vehicle is already parking")
            return
        }
        print("Vehicle is now parking")
        self.parking = true
        DispatchQueue.global().asyncAfter(deadline: .now() + 5) {
            self.stopParking()
        }
    }
    
    private func stopParking() {
        print("Vehicle has stopped parking")
        self.parking = false
        if let stoppedState = self.vehicle?.getStoppedState() {
            self.vehicle?.setState(stoppedState)
        }
    }
}
//let vehicle = VehicleContext()
//vehicle.breakMotor()
//vehicle.accelerate()
//vehicle.accelerate()
//vehicle.breakMotor()
//vehicle.park() // prints: Can't park the vehicle while it's moving. You need to stop first
//vehicle.breakMotor()
//vehicle.park()


final class Context {
    
    private var state: AuthorizationState = UnauthorizedState()
    
    var isAuthorized: Bool {
        return state.isAuthorized(self)
    }
    var userId: String? {
        return state.userId(self)
    }
    var password: String? {
        return state.password(self)
    }
    
    func changeStateToAuthorized(with userId: String) {
        state = AuthorizedState(userId: userId)
    }
    
    func changeStateToUnathorized() {
        state = UnauthorizedState()
    }
    
    func changeStateToPassword(_ password: String) {
        state = PasswordState(password: password)
    }
}
protocol AuthorizationState {
    func isAuthorized(_ context: Context) -> Bool
    func userId(_ context: Context) -> String?
    func password(_ context: Context) -> String?
}
class PasswordState: AuthorizationState {
    
    let password: String
    
    init(password: String) {
        self.password = password
    }
    
    func isAuthorized(_ context: Context) -> Bool {
        return false
    }
    
    func userId(_ context: Context) -> String? {
        return context.userId
    }
    
    func password(_ context: Context) -> String? {
        return password
    }
}
class UnauthorizedState: AuthorizationState {
    
    func isAuthorized(_ context: Context) -> Bool {
        return false
    }
    
    func userId(_ context: Context) -> String? {
        return ""
    }
    
    func password(_ context: Context) -> String? {
        return ""
    }
}
class AuthorizedState: AuthorizationState {
    
    let userId: String
    
    init(userId: String) {
        self.userId = userId
    }
    
    func isAuthorized(_ context: Context) -> Bool {
        if context.password != "" {
            return true
        }
        return context.isAuthorized
    }
    
    func userId(_ context: Context) -> String? {
        return userId
    }
    
    func password(_ context: Context) -> String? {
        return context.password
    }
}
//let context = Context()
//context.isAuthorized
//context.userId
//context.changeStateToAuthorized(with: "Vladymyr")
//context.isAuthorized
//context.userId

