//: [Previous](@previous)

import Foundation

//MARK:-Iterator
protocol IteratorProtocol {
    associatedtype Element
    mutating func next() -> Self.Element?
}
protocol Sequence {
    associatedtype Iterator : IteratorProtocol
    func makeIterator() -> Self.Iterator
    // Another requirements go here…
}
struct Book {
    let author: String
    let title: String
}
struct Shelf {
    var books: [Book]
}
extension Shelf: Sequence {
    func makeIterator() -> ShelfIterator {
        return ShelfIterator(books: books)
    }
}
protocol ClassicalIteratorProtocol: IteratorProtocol {
    var currentItem: Element? { get }
    var first: Element? { get }
    var isDone: Bool { get }
}
struct ShelfIterator: ClassicalIteratorProtocol {
    
    var currentItem: Book? = nil
    var first: Book?
    var isDone: Bool = false
    
    private var books: [Book]
    
    init(books: [Book]) {
        self.books = books
        self.first = books.first
        self.currentItem = books.first
    }
    
    mutating func next() -> Book? {
        currentItem = books.first
        books.removeFirst()
        isDone = books.isEmpty
        
        return books.first
    }
}

let book1 = Book(author: "Den Brown", title: "Inferno")
let book2 = Book(author: "Den Brown", title: "Code Davinci")
let book3 = Book(author: "Den Brown", title: "Angels And Deamons")
let book4 = Book(author: "Den Brown", title: "Point of lie")

let shelf = Shelf(books: [book1, book2, book3, book4])

func printShelf(with iterator: inout ShelfIterator) {
    var bookIndex = 0
    while !iterator.isDone {
        bookIndex += 1
        print("\(bookIndex). \(iterator.currentItem!.author) – \(iterator.currentItem!.title)")
        _ = iterator.next()
    }
}
var iterator = ShelfIterator(books: shelf.books)
printShelf(with: &iterator)
