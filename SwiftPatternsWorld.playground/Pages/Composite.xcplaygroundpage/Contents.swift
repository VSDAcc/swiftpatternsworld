
import Foundation
import UIKit

//MARK:-Composite
protocol Payee {
    var name: String { get }
    var bonusAmount: Double { get }
    func receiveBonus(_ bonus: Double)
}
class Employee: Payee {
    
    private var _name: String
    private var _bonusAmount: Double = 0.0
    
    var name: String {
        return _name
    }
    var bonusAmount: Double {
        return _bonusAmount
    }
    
    init(name: String) {
        self._name = name
    }
    
    func receiveBonus(_ bonus: Double) {
        self._bonusAmount += bonus
    }
}
class Department: Payee {
    
    private var _name: String
    private var _bonusAmount: Double {
        get {
            var bonus: Double = 0.0
            subunits.forEach { (subunit) in
                bonus += subunit.bonusAmount
            }
            return bonus
        } set {
            let splitCount = Double(subunits.count)
            let splitBonus = newValue / splitCount
            subunits.forEach { (subinit) in
                print(splitBonus)
                subinit.receiveBonus(splitBonus)
            }
        }
    }
    
    var name: String {
        return _name
    }
    var bonusAmount: Double {
        return _bonusAmount
    }
    private var subunits: [Payee] = []
    
    init(name: String, subunits: [Payee]) {
        self._name = name
        self.subunits = subunits
    }
    
    func addSubunit(_ payye: Payee) {
        self.subunits.append(payye)
    }
    
    func receiveBonus(_ bonus: Double) {
        self._bonusAmount += bonus
    }
}
let joan = Employee(name: "Joan")
let tom = Employee(name: "Tom")
let cleo = Employee(name: "Cleo")
let alex = Employee(name: "Alex")

let graphicsDesignDepartment = Department(name: "Graphics", subunits: [joan, tom, cleo, alex])
graphicsDesignDepartment.bonusAmount
graphicsDesignDepartment.receiveBonus(1000.0)
alex.bonusAmount
let musicJoe = Employee(name: "musicJoe")
let musicChloe = Employee(name: "musicChloe")
let musicDepartment = Department(name: "Music", subunits: [musicJoe, musicChloe, graphicsDesignDepartment])
musicDepartment.receiveBonus(1000.0)

