

import UIKit
import Foundation
import PlaygroundSupport

let higherPriority = DispatchQueue.global(qos: .userInteractive)
let lowerPriority = DispatchQueue.global(qos: .userInitiated)

let semaphore = DispatchSemaphore(value: 1)
let groupOne = DispatchGroup()
let groupTwo = DispatchGroup()

func asyncPrint(queue: DispatchQueue, symbol: String) { // wait - блокирует поток и ждет пока операции завершатся на этом потоке
    queue.async {
        print("\(symbol) waiting")
        semaphore.wait()
        
        for i in 0...10 {
            print(symbol, i)
        }
        
        print("\(symbol) signal")
        semaphore.signal()
    }
}
//asyncPrint(queue: lowerPriority, symbol: "🔵")
//asyncPrint(queue: higherPriority, symbol: "🔴")

func asyncGroupPrint(group: DispatchGroup, symbol: String) { // group - добавляет операции и опевещает о завершении всех операций в нотификации
    group.enter()
    print("\(symbol) enter")
    
    for i in 0...10 {
        print(symbol, i)
    }
    group.leave()
    group.notify(queue: .main) {
        print("\(symbol) notify")
    }
}

//asyncGroupPrint(group: groupTwo, symbol: "🔵")
//asyncGroupPrint(group: groupOne, symbol: "🔴")

PlaygroundPage.current.needsIndefiniteExecution = true


protocol Idenitifiable {
    var cellIdentifier: String { get }
}
class CellIdentifiable: NSObject, Idenitifiable {
    
    var cellIdentifier: String {
        return ""
    }
    
    static func == (lhs: CellIdentifiable, rhs: CellIdentifiable) -> Bool {
        return isEqual(rhs)
    }

    override func isEqual(_ object: Any?) -> Bool {
        guard let rhs = object as? CellIdentifiable else {
            return false
        }
        let lhs = self
        return lhs.cellIdentifier == rhs.cellIdentifier
    }
}
class Cell1: CellIdentifiable {
    
    var id: Int = 1
    
    override var cellIdentifier: String {
        return "helloWorld"
    }
    
    init(_ id: Int) {
        self.id = id
    }
    
    override var hash: Int {
        return id
    }

    override func isEqual(_ object: Any?) -> Bool {
        if let other = object as? Cell1 {
            return self.id == other.id
        } else {
            return false
        }
    }
}


let first = Cell1(1)
let second = Cell1(1)
let third = Cell1(1)
let set = Set([first, second, third])
set.count
first == second
