
import Foundation
import UIKit

//MARK:-Strategy
protocol CompressorStrategy {
    func compress()
}
class Compressor: CompressorStrategy {
    
    public var compressor: CompressorStrategy
    
    init(compressor: CompressorStrategy) {
        self.compressor = compressor
    }
    
    func compress() {
        compressor.compress()
    }
}
class RARCompressor: CompressorStrategy {
    
    func compress() {
        print("Compression RAR arhive")
    }
}
class ZIPCompressor: CompressorStrategy {
    
    func compress() {
        print("Compression ZIP arhive")
    }
}
class ARJCompressor: CompressorStrategy {
    
    func compress() {
        print("Compression ARJ arhive")
    }
}
let rarCompressor = RARCompressor()
let compressor = Compressor(compressor: ZIPCompressor())
compressor.compress()
compressor.compressor = rarCompressor
compressor.compress()

