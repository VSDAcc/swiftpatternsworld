
import Foundation
//MARK:-Adapter

protocol TemperatureSensor {
    func getTemperature() -> Float
}
class FarenheitSensor: TemperatureSensor {
    
    private var temperature: Float = 451.0
    
    func getTemperature() -> Float {
        return temperature
    }
}
protocol CelsuimAdapter {
    func getCelsiumTemperature() -> Double
}
class CelsiumToFarenheitAdapter: CelsuimAdapter {
    
    private var farenheitSensor: TemperatureSensor
    
    init(farenheitSensor: TemperatureSensor) {
        self.farenheitSensor = farenheitSensor
    }
    
    func getCelsiumTemperature() -> Double {
        return Double((farenheitSensor.getTemperature() - 32) * 5.0 / 9.0)
    }
}
protocol ClimatSensor {
    func getFarenheitTemperature() -> Float
    func getCelsiumTemperature() -> Double
}
class HouseSensor: ClimatSensor {
    
    private var temperatureSensor: TemperatureSensor
    private var celsiumAdapter: CelsuimAdapter
    
    init(temperatureSensor: TemperatureSensor) {
        self.temperatureSensor = temperatureSensor
        self.celsiumAdapter = CelsiumToFarenheitAdapter(farenheitSensor: temperatureSensor)
    }
    
    func getFarenheitTemperature() -> Float {
        return temperatureSensor.getTemperature()
    }
    
    func getCelsiumTemperature() -> Double {
        return celsiumAdapter.getCelsiumTemperature()
    }
}
let farenheitSensor = FarenheitSensor()
let houseSensor = HouseSensor(temperatureSensor: farenheitSensor)
houseSensor.getCelsiumTemperature()
houseSensor.getFarenheitTemperature()
