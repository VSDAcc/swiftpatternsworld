//: [Previous](@previous)

import Foundation

//MARK:-Mediator
protocol DevelopmentMediator {
    func sendMessage(_ message: String, sender: DevelopmentColleague)
}

protocol DevelopmentColleague {
    var id: String {get set}
    var mediator: DevelopmentMediator {get set}
    func sendMessage(_ message: String)
    func recieveMessage(_ message: String)
}
class IOSDeveloper: DevelopmentColleague {
    
    var id: String
    var mediator: DevelopmentMediator
    
    init(id: String, mediator: DevelopmentMediator) {
        self.id = id
        self.mediator = mediator
    }
    
    func sendMessage(_ message: String) {
        mediator.sendMessage(message, sender: self)
    }
    
    func recieveMessage(_ message: String) {
        print("Message recieved \(message) \(id)")
    }
}

class AndroidDeveloper: DevelopmentColleague {
    
    var id: String
    var mediator: DevelopmentMediator
    
    init(id: String, mediator: DevelopmentMediator) {
        self.id = id
        self.mediator = mediator
    }
    
    func sendMessage(_ message: String) {
        mediator.sendMessage(message, sender: self)
    }
    
    func recieveMessage(_ message: String) {
        print("Message recieved \(message) \(id)")
    }
}

protocol DevelopmentDirectorDelegate {
    var colleagues: [DevelopmentColleague] {get set}
    func registerColleagua(_ colleagua: DevelopmentColleague)
}

class DevelopmentDirector: DevelopmentDirectorDelegate {
    
    var colleagues: [DevelopmentColleague] = []
    
    func registerColleagua(_ colleagua: DevelopmentColleague) {
        colleagues.append(colleagua)
    }
}

class DevelopmentMessangerMediator: DevelopmentMediator {
    
    var director: DevelopmentDirectorDelegate
    
    init(_ director: DevelopmentDirectorDelegate) {
        self.director = director
    }
    
    func sendMessage(_ message: String, sender: DevelopmentColleague) {
        for colleague in director.colleagues {
            if colleague.id != sender.id {
                colleague.recieveMessage(message)
            }
        }
    }
}
let director = DevelopmentDirector()
let mediator = DevelopmentMessangerMediator(director)
let iosDev = IOSDeveloper(id: "ios", mediator: mediator)
let androidDev = AndroidDeveloper(id: "android", mediator: mediator)
director.registerColleagua(iosDev)
director.registerColleagua(androidDev)
iosDev.sendMessage("Hello")
