
import Foundation
//MARK:-Builder

protocol Infantryman {
    var healthPoints: Float { get set }
    var sword: String { get set }
    func move()
    func attack()
}
protocol Archer {
    var healthPoints: Float { get set }
    var bow: String { get set }
    func move()
    func attack()
}
protocol Horseman {
    var healthPoints: Float { get set }
    var horse: String { get set }
    func move()
    func attack()
}
struct RomanInfantryman: Infantryman {
    
    var healthPoints: Float
    var sword: String
    
    func move() {
        print("Roman infantryman move")
    }
    
    func attack() {
        print("Roman infantryman attack")
    }
}
struct RomanArcher: Archer {
    
    var healthPoints: Float
    var bow: String
    
    func move() {
        print("Roman archer move")
    }
    
    func attack() {
        print("Roman archer attack")
    }
}
struct RomanHorseman: Horseman {
    
    var healthPoints: Float
    var horse: String
    
    func move() {
        print("Roman horseman move")
    }
    
    func attack() {
        print("Roman horseman attack")
    }
}
protocol InfantrymanBuilder {
    func reset()
    func setHealt(_ healt: Float)
    func setSword(_ sword: String)
    func getResult() -> Infantryman
}
class RomanInfantrymanBuilder: InfantrymanBuilder {
    
    private var infantryman: Infantryman = RomanInfantryman(healthPoints: 100.0, sword: "Sword")
    
    func reset() {
        infantryman = RomanInfantryman(healthPoints: 100.0, sword: "Sword")
    }
    
    func setHealt(_ healt: Float) {
        infantryman.healthPoints = healt
    }
    
    func setSword(_ sword: String) {
        infantryman.sword = sword
    }
    
    func getResult() -> Infantryman {
        return infantryman
    }
}
protocol ArcherBuilder {
    func reset()
    func setHealt(_ healt: Float)
    func setBow(_ sword: String)
    func getResult() -> Archer
}
class RomanArcherBuilder: ArcherBuilder {
    
    private var archer: Archer = RomanArcher(healthPoints: 100.0, bow: "Bow")
    
    func reset() {
        archer = RomanArcher(healthPoints: 100.0, bow: "Bow")
    }
    
    func setHealt(_ healt: Float) {
        archer.healthPoints = healt
    }
    
    func setBow(_ bow: String) {
        archer.bow = bow
    }
    
    func getResult() -> Archer {
        return archer
    }
}
protocol ArmyDirectorProtocol {
    func setArcherBuilder(_ builder: ArcherBuilder)
    func setInfantrymanBuilder(_ builder: InfantrymanBuilder)
    func createInfantryman(with health: Float, sword: String) -> Infantryman
    func createArcher(with health: Float, bow: String) -> Archer
}
class ArmyDirector {
    
    private var archerBuilder: ArcherBuilder
    private var infantrymanBuilder: InfantrymanBuilder
    
    init(archerBuilder: ArcherBuilder, infantrymanBuilder: InfantrymanBuilder) {
        self.archerBuilder = archerBuilder
        self.infantrymanBuilder = infantrymanBuilder
    }
    
    func setArcherBuilder(_ builder: ArcherBuilder) {
        self.archerBuilder = builder
    }
    
    func setInfantrymanBuilder(_ builder: InfantrymanBuilder) {
        self.infantrymanBuilder = builder
    }
    
    func createInfantryman(with health: Float, sword: String) -> Infantryman {
        infantrymanBuilder.reset()
        infantrymanBuilder.setHealt(health)
        infantrymanBuilder.setSword(sword)
        return infantrymanBuilder.getResult()
    }
    
    func createArcher(with health: Float, bow: String) -> Archer {
        archerBuilder.reset()
        archerBuilder.setHealt(health)
        archerBuilder.setBow(bow)
        return archerBuilder.getResult()
    }
}
let romanArcher = RomanArcherBuilder()
let romanInfantryman = RomanInfantrymanBuilder()
let director = ArmyDirector(archerBuilder: romanArcher, infantrymanBuilder: romanInfantryman)
let archer = director.createArcher(with: 50, bow: "long bow")
let infantryman = director.createInfantryman(with: 90, sword: "Excalibur")
archer.bow
infantryman.sword
