
import Foundation

//MARK:-Bridge
protocol Car {
    func drive()
}
class PassangerCar: Car {
    
    func drive() {
        print("drive passanger")
    }
}
class BusCar: Car {
    
    func drive() {
        print("drive bus")
    }
}

class TruckCar: Car {
    
    func drive() {
        print("drive truck")
    }
}

protocol CarModel {
    var car: Car {get set}
    var model: String {get set}
}
class BMW: CarModel {
    
    var car: Car
    var model: String
    
    init(car: Car, model: String) {
        self.car = car
        self.model = model
    }
}
class Woltsvagen: CarModel {
    
    var car: Car
    var model: String
    
    init(car: Car, model: String) {
        self.car = car
        self.model = model
    }
}

let passangerCar = PassangerCar()
let bmwCar = BMW(car: passangerCar, model: "x7")
bmwCar.car.drive()
