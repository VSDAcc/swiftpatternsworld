
import Foundation
import UIKit

//MARK:-Command
protocol ChessGame {
    func create()
    func open()
    func save()
    func move()
}
class ChessFile: ChessGame {
    
    func create() {
        print("Game creted")
    }
    
    func open() {
        print("Game opened")
    }
    
    func save() {
        print("Game saved")
    }
    
    func move() {
        print("Game moved")
    }
}

protocol ChessCommand {
    var chess: ChessGame { get }
    func execute()
}
class CreateChessGame: ChessCommand {
    
    let chess: ChessGame
    
    init(chess: ChessGame) {
        self.chess = chess
    }
    
    func execute() {
        chess.create()
    }
}
class OpenChessGame: ChessCommand {
    
    let chess: ChessGame
    
    init(chess: ChessGame) {
        self.chess = chess
    }
    
    func execute() {
        chess.open()
    }
}
class SaveChessGame: ChessCommand {
    
    let chess: ChessGame
    
    init(chess: ChessGame) {
        self.chess = chess
    }
    
    func execute() {
        chess.save()
    }
}
class MoveChessGame: ChessCommand {
    
    let chess: ChessGame
    
    init(chess: ChessGame) {
        self.chess = chess
    }
    
    func execute() {
        chess.move()
    }
}
protocol MoveChessToPlace: ChessCommand {
    var place: String { get set }
}
class MoveCheeToCache: MoveChessToPlace {
    
    var chess: ChessGame
    var place: String
    
    init(chess: ChessGame, place: String) {
        self.chess = chess
        self.place = place
    }
    
    func execute() {
        print("moved to cache")
        chess.move()
    }
}
protocol ChessGameBuilder {
    func setCommand(_ command: ChessCommand)
    func execute()
}
class ChessMatch: ChessGameBuilder {

    fileprivate var command: ChessCommand
    
    init(command: ChessCommand) {
        self.command = command
    }
    
    public func setCommand(_ command: ChessCommand) {
        self.command = command
    }
    
    public func execute() {
        self.command.execute()
    }
}
let chessFile = ChessFile()
let chessMatch = ChessMatch(command: OpenChessGame(chess: chessFile))
let createCommand = CreateChessGame(chess: chessFile)
chessMatch.setCommand(createCommand)
chessMatch.execute()

