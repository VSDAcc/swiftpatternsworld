//: [Previous](@previous)

import Foundation

//MARK:-Prototype
extension Array {
    func contains<T>(obj: T) -> Bool where T : Equatable {
        return self.filter({$0 as? T == obj}).count > 0
    }
}

protocol BooksDirectory {
    func appendPage(_ page: Page)
    func copyPage(_ page: Page) -> Page?
}
class AuthorBookDirectory: BooksDirectory {
    
    private var pages: [Page] = []
    
    func appendPage(_ page: Page) {
        pages.append(page)
    }
    
    func copyPage(_ page: Page) -> Page? {
        var copyPage: Page?
        pages.forEach { (arrayPage) in
            if arrayPage.title == page.title {
                copyPage = arrayPage.copy()
            }
        }
        return copyPage
    }
}
protocol Author {
    var id: String { get set }
    var name: String { get set }
    func setBookDirectory(_ directory: BooksDirectory)
    func writePage(_ page: Page)
    func copyPage(_ page: Page) -> Page?
}
class AghataKristi: Author {
    
    var id: String
    var name: String
    private var directory: BooksDirectory?
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    
    func setBookDirectory(_ directory: BooksDirectory) {
        self.directory = directory
    }
    
    func writePage(_ page: Page) {
        directory?.appendPage(page)
    }
    
    func copyPage(_ page: Page) -> Page? {
        return directory?.copyPage(page)
    }
}
protocol Page {
    var title: String { get set }
    var contents: String { get set }
    var author: Author? { get set }
    func copy() -> Page
}
class TitlePage: Page, NSCopying {
    
    var title: String
    var contents: String
    var author: Author?
    
    init(title: String, contents: String, author: Author? = nil) {
        self.title = title
        self.contents = contents
        self.author = author
    }
    
    func setAuthor(_ author: Author) {
        self.author = author
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        return TitlePage(title: "Copy of '" + title + "'", contents: contents, author: author)
    }
    
    func copy() -> Page {
        return copy(with: nil) as! TitlePage
    }
}
class Book {
    
    fileprivate var author: Author
    fileprivate let directory: BooksDirectory
    
    init(author: Author, directory: BooksDirectory) {
        self.author = author
        self.directory = directory
        self.author.setBookDirectory(directory)
    }
    
    func appendPage(_ page: Page) {
        author.writePage(page)
    }
    
    func copyPage(_ page: Page) -> Page? {
        return author.copyPage(page)
    }
}
let murderInExpressDirectory = AuthorBookDirectory()
let aghataKristi = AghataKristi(id: "Aghata", name: "Kristi")
let murderInEstrenExpress = Book(author: aghataKristi, directory: murderInExpressDirectory)
let firstTitlePage = TitlePage(title: "Eastern Express", contents: "Murder in express", author: aghataKristi)
murderInEstrenExpress.appendPage(firstTitlePage)
let copyFirstPage = murderInEstrenExpress.copyPage(firstTitlePage)
print(copyFirstPage?.title ?? "nil")

