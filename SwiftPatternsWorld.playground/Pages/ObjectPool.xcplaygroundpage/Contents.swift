
import Foundation
//MARK:-ObjectPool

class ObjectPool<T> {
    
    private let lockQueue = DispatchQueue(label: "objectPool.lock.queue")
    private let semaphore: DispatchSemaphore
    public private(set) var items: [T] = []
    
    init(items: [T]) {
        self.semaphore = DispatchSemaphore(value: items.count)
        self.items.reserveCapacity(items.count)
        self.items.append(contentsOf: items)
    }
    
    public func acquire() -> T? {
        if semaphore.wait(timeout: .distantFuture) == .success, !items.isEmpty {
            return self.lockQueue.sync {
                return self.items.remove(at: 0)
            }
        }
        return nil
    }
    
    public func release(_ item: T) {
        self.lockQueue.sync {
            self.items.append(item)
            self.semaphore.signal()
        }
    }
}
let pool = ObjectPool<String>(items: ["a", "b", "c", "d"])
let a = pool.acquire()
print(a ?? "n/a acquired")
let b = pool.acquire()
print(b ?? "n/a acquired")
let c = pool.acquire()
print(c ?? "n/a acquired")

DispatchQueue.global(qos: .default).asyncAfter(deadline: .now() + .seconds(2)) {
    if let item = b {
        pool.release(item)
    }
}
print("No more resource in the pool, blocking thread until...")
let x = pool.acquire()
print("\(x ?? "n/a") acquired again")
