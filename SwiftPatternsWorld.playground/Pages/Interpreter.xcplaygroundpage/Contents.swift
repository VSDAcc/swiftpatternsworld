
import Foundation

//MARK:-Interpreter

protocol Expression {
    func interpret(_ variables: [String: Expression]) -> Double
}
class Number: Expression {
    
    var value: Double
    
    init(value: Double) {
        self.value = value
    }
    
    func interpret(_ variables: [String: Expression]) -> Double {
        return value
    }
}
class Add: Expression {
    
    var leftOperand: Expression
    var rightOperand: Expression
    
    init(leftOperand: Expression, rightOperand: Expression) {
        self.leftOperand = leftOperand
        self.rightOperand = rightOperand
    }
    
    func interpret(_ variables: [String: Expression]) -> Double {
        return leftOperand.interpret(variables) + rightOperand.interpret(variables)
    }
}
class Subtract: Expression {
    
    var leftOperand: Expression
    var rightOperand: Expression
    
    init(leftOperand: Expression, rightOperand: Expression) {
        self.leftOperand = leftOperand
        self.rightOperand = rightOperand
    }
    
    func interpret(_ variables: [String : Expression]) -> Double {
        return leftOperand.interpret(variables) - rightOperand.interpret(variables)
    }
}

class Multiply: Expression {
    
    var leftOperand: Expression
    var rightOperand: Expression
    
    init(leftOperand: Expression, rightOperand: Expression) {
        self.leftOperand = leftOperand
        self.rightOperand = rightOperand
    }
    
    func interpret(_ variables: [String : Expression]) -> Double {
        return leftOperand.interpret(variables) * rightOperand.interpret(variables)
    }
}

class Divide: Expression {
    
    var leftOperand: Expression
    var rightOperand: Expression
    
    init(leftOperand: Expression, rightOperand: Expression) {
        self.leftOperand = leftOperand
        self.rightOperand = rightOperand
    }
    
    func interpret(_ variables: [String : Expression]) -> Double {
        return leftOperand.interpret(variables) / rightOperand.interpret(variables)
    }
}
class Variable: Expression {
    private let name: String
    
    init(name: String) {
        self.name = name
    }
    
    func interpret(_ variables: [String: Expression]) -> Double {
        if let expression = variables[name] {
            return expression.interpret(variables)
        } else {
            return 0.0
        }
    }
}
class Evaluator: Expression {
    
    var syntaxTree: Expression = Number(value: 0.00)
    var expression: String
    
    init(expression: String) {
        self.expression = expression
    }
    
    private func buildSyntaxTree() {
        var expressionStack = Stack<Expression>()
        
        var items = expression.components(separatedBy:" ")
        
        var index = 0
        
        while index < items.count {
            
            switch items[index] {
            case "*":
                let nextExpression = getNextExpression(items: items, index: index)
                expressionStack.push(item: Multiply(leftOperand: expressionStack.pop(),
                                              rightOperand: nextExpression))
                index += 2
            case "/":
                let nextExpression = getNextExpression(items: items, index: index)
                expressionStack.push(item: Divide(leftOperand: expressionStack.pop(),
                                            rightOperand: nextExpression))
                index += 2
            case "+":
                let nextExpression = getNextExpression(items: items, index: index)
                expressionStack.push(item: Add(leftOperand: expressionStack.pop(),
                                         rightOperand: nextExpression))
                index += 2
                
            case "-":
                let nextExpression = getNextExpression(items: items, index: index)
                expressionStack.push(item: Subtract(leftOperand: expressionStack.pop(),
                                              rightOperand: nextExpression))
                index += 2
                
            default:
                if let doubleValue = Double(items[index]) {
                    expressionStack.push(item: Number(value: doubleValue))
                    index += 1
                } else {
                    expressionStack.push(item: Variable(name: items[index]))
                    index += 1
                }
            }
            
        }
        syntaxTree = expressionStack.pop()
    }
    
    private func getNextExpression(items: [String], index: Int) -> Expression {
        let next = items[index + 1]
        var nextExpression: Expression
        if let doubleValue = Double(next) {
            nextExpression = Number(value: doubleValue)
        } else {
            nextExpression = Variable(name: next)
        }
        return nextExpression
    }
    
    func interpret(_ variables: [String : Expression]) -> Double {
        
        if (expression.contains("/") || expression.contains("*")) &&
            (expression.contains("+") || expression.contains("-")) {
            
            let expressions = parseoutAdditionsAndSubtractions(input: expression)
            var newExpression = ""
            var index = 0
            for expression in expressions {
                if expression == "+" || expression == "-" {
                    newExpression += expression
                } else {
                    let eval = Evaluator(expression: expression)
                    let result = eval.interpret(variables)
                    newExpression += String(result)
                }
                
                if index != expressions.count - 1 {
                    newExpression += " "
                }
                index += 1
            }
            let evaluator = Evaluator(expression: newExpression)
            return evaluator.interpret(variables)
        } else {
            buildSyntaxTree()
            return syntaxTree.interpret(variables)
        }
    }
    
    private func parseoutAdditionsAndSubtractions(input: String) -> [String] {
        var result = [String]()
        
        let items = input.components(separatedBy: " ")
        
        var sentence = ""
        var index = 0
        for item in items {
            if item == "+" || item == "-" {
                result.append(sentence.trimmingCharacters(in: .whitespacesAndNewlines))
                result.append(item)
                sentence = ""
            } else {
                sentence += item
                if index != items.count - 1 {
                    sentence += " "
                }
            }
            index += 1
        }
        result.append(sentence)
        return result
    }
}
struct Stack<T> {
    
    var items = [T]()
    
    mutating func push(item: T) {
        items.append(item)
    }
    
    mutating func pop() -> T {
        return items.removeLast()
    }
}
class Quote {
    
    var partsPrice: Double
    var laborPrice: Double
    var adjustments: String?
    
    init(partsPrice: Double, laborPrice: Double, adjustments: String?) {
        self.partsPrice = partsPrice
        self.laborPrice = laborPrice
        self.adjustments = adjustments
    }
    
    convenience init(partsPrice: Double, laborPrice: Double) {
        self.init(partsPrice: partsPrice, laborPrice: laborPrice, adjustments: nil)
    }
    
    var totalPrice: Double {
        if let adjustments = adjustments {
            var variables = [String: Expression] ()
            variables["l"] = Number(value: laborPrice)
            variables["p"] = Number(value: partsPrice)
            let evaluator = Evaluator(expression: adjustments)
            return evaluator.interpret(variables)
        } else {
            return partsPrice + laborPrice
        }
    }
}
