//: [Previous](@previous)

import Foundation
import UIKit

//MARK:-Flyweight
protocol Car {
    var color: UIColor { get set }
    func drive()
}
class Sedan: Car, Equatable {
    
    var color: UIColor
    
    init(_ color: UIColor) {
        self.color = color
    }
    
    func drive() {
        print("driving")
    }
    
    static func == (lhs: Sedan, rhs: Sedan) -> Bool {
        return lhs.color == rhs.color
    }
}

protocol CarFactoryProtocol {
    func getCar(_ color: UIColor) -> Car
    func clearCache()
}
class SedanFactory: CarFactoryProtocol {
    
    private var cars: [UIColor: Car] = [UIColor: Car]()
    
    func getCar(_ color: UIColor) -> Car {
        if let car = cars[color] {
            return car
        } else {
            let car = Sedan(color)
            cars[color] = car
            return car
        }
    }
    
    func clearCache() {
        cars = [:]
    }
}
let sedanFactory = SedanFactory()
let sedanCar1: Sedan = sedanFactory.getCar(.blue) as! Sedan
let sedanCar2: Sedan = sedanFactory.getCar(.blue) as! Sedan
sedanCar1 === sedanCar2

