
import Foundation
//MAKR:-Abstract Factory

protocol Infantryman {
    var healthPoints: Float { get set }
    var sword: String { get set }
    func move()
    func attack()
}
protocol Archer {
    var healthPoints: Float { get set }
    var bow: String { get set }
    func move()
    func attack()
}
protocol Horseman {
    var healthPoints: Float { get set }
    var horse: String { get set }
    func move()
    func attack()
}
struct RomanInfantryman: Infantryman {
    
    var healthPoints: Float
    var sword: String
    
    func move() {
        print("Roman infantryman move")
    }
    
    func attack() {
       print("Roman infantryman attack")
    }
}
struct RomanArcher: Archer {
    
    var healthPoints: Float
    var bow: String
    
    func move() {
        print("Roman archer move")
    }
    
    func attack() {
        print("Roman archer attack")
    }
}
struct RomanHorseman: Horseman {
    
    var healthPoints: Float
    var horse: String
    
    func move() {
        print("Roman horseman move")
    }
    
    func attack() {
        print("Roman horseman attack")
    }
}
struct CarthaginianInfantryman: Infantryman {
    
    var healthPoints: Float
    var sword: String
    
    func move() {
        print("Carthaginian infantryman move")
    }
    
    func attack() {
        print("Carthaginian infantryman attack")
    }
}
struct CarthaginianArcher: Archer {
    
    var healthPoints: Float
    var bow: String
    
    func move() {
        print("Carthaginian archer move")
    }
    
    func attack() {
        print("Carthaginian archer attack")
    }
}
struct CarthaginianHorseman: Horseman {
    
    var healthPoints: Float
    var horse: String
    
    func move() {
        print("Carthaginian horseman move")
    }
    
    func attack() {
        print("Carthaginian horseman attack")
    }
}
protocol ArmyFactoryProtocol {
    func createInfantryman() -> Infantryman
    func createArcher() -> Archer
    func createHorseman() -> Horseman
}
class ArmyFactory: ArmyFactoryProtocol {
    
    var nation: ArmyFactoryProtocol
    
    init(nation: ArmyFactoryProtocol) {
        self.nation = nation
    }
    
    func createInfantryman() -> Infantryman {
        return nation.createInfantryman()
    }
    
    func createArcher() -> Archer {
        return nation.createArcher()
    }
    
    func createHorseman() -> Horseman {
        return nation.createHorseman()
    }
}
class RomanArmyFactory: ArmyFactoryProtocol {
    
    func createInfantryman() -> Infantryman {
        return RomanInfantryman(healthPoints: 80.0, sword: "Fellbringer")
    }
    
    func createArcher() -> Archer {
        return RomanArcher(healthPoints: 60.0, bow: "Felobow")
    }
    
    func createHorseman() -> Horseman {
        return RomanHorseman(healthPoints: 100.0, horse: "Horsen")
    }
}
class CarthaginianArmyFactory: ArmyFactoryProtocol {
    
    func createInfantryman() -> Infantryman {
        return CarthaginianInfantryman(healthPoints: 80.0, sword: "Fellbringer")
    }
    
    func createArcher() -> Archer {
        return CarthaginianArcher(healthPoints: 60.0, bow: "Felobow")
    }
    
    func createHorseman() -> Horseman {
        return CarthaginianHorseman(healthPoints: 100.0, horse: "Horsen")
    }
}
class Army {
    
    private let armyFactory: ArmyFactory
    var infantryman: Infantryman?
    var archer: Archer?
    var hourseman: Horseman?
    
    init(_ nation: ArmyFactoryProtocol) {
        self.armyFactory = ArmyFactory(nation: nation)
    }
    
    func createArmy() {
        infantryman = armyFactory.createInfantryman()
        archer = armyFactory.createArcher()
        hourseman = armyFactory.createHorseman()
    }
}
class Game {
    
    var army: Army
    
    init(army: Army) {
        self.army = army
    }
    
    func createArmy() {
        army.createArmy()
    }
}
let carthaginianArmy = Army(CarthaginianArmyFactory())
let romanArmy = Army(RomanArmyFactory())
let game = Game(army: romanArmy)
game.createArmy()
game.army.archer?.attack()
game.army.archer?.move()
