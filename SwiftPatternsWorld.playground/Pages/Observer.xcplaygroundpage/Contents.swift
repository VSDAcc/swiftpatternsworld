//: [Previous](@previous)

import Foundation
import UIKit

//MARK:-Observer
protocol AuctioneerPublisher: class {
    func notify(from listener: AuctionBidder)
}
class Auctioneer: AuctioneerPublisher {
    
    fileprivate let director: AuctioneerDirectorDelegate
    
    init(director: AuctioneerDirectorDelegate) {
        self.director = director
    }
    
    func notify(from: AuctionBidder) {
        print("Gentlemans we have new bid \(from.bidd) from \(from.name)")
        director.makeBidd(from)
    }
}
protocol AuctionBidder {
    func update(bid: Float)
    func makeBid()
    var bidd: Float { get set }
    var name: String { get set }
}
class Bidder: AuctionBidder {
    
    var bidd: Float = 0
    var name: String
    
    init(name: String) {
        self.name = name
    }
    
    func update(bid: Float) {
        print("\(name) recieve a new bid \(bid)")
        self.bidd = bid
    }
    
    func makeBid() {
        let bid = Float(arc4random() % 1000)
        self.bidd = bid
    }
}
protocol AuctioneerDirectorDelegate {
    func appendListener(_ listener: AuctionBidder)
    func removeListener(_ listener: AuctionBidder)
    func makeBidd(_ listener: AuctionBidder)
}
class AuctionDirector: AuctioneerDirectorDelegate {
    
    private var listeners: NSMutableArray = []
    
    public func appendListener(_ listener: AuctionBidder) {
        listeners.add(listener)
    }
    
    public func removeListener(_ listener: AuctionBidder) {
        listeners.remove(listener)
    }
    
    public func makeBidd(_ listener: AuctionBidder) {
        listeners.forEach { (auctionBidders) in
            (auctionBidders as? AuctionBidder)?.update(bid: listener.bidd)
        }
    }
}
class Auction {
    
    fileprivate var auctioneer: AuctioneerPublisher
    fileprivate var director: AuctioneerDirectorDelegate
    
    init(director: AuctioneerDirectorDelegate) {
        self.director = director
        self.auctioneer = Auctioneer(director: director)
    }
    
    func addBidder(_ bidder: AuctionBidder) {
        director.appendListener(bidder)
    }
    
    func removeBidder(_ bidder: AuctionBidder) {
        director.removeListener(bidder)
    }
    
    func makeBidd(from bidder: AuctionBidder) {
        bidder.makeBid()
        auctioneer.notify(from: bidder)
    }
}
let moneDirector = AuctionDirector()
let moneAuction = Auction(director: moneDirector)
var fancyBidder = Bidder(name: "Bob Rosh")
var pureBidder = Bidder(name: "Pure Charly")
var maddamBidder = Bidder(name: "Dragomirova maddam")
moneAuction.addBidder(maddamBidder)
moneAuction.addBidder(pureBidder)
moneAuction.addBidder(fancyBidder)
moneAuction.makeBidd(from: maddamBidder)
moneAuction.makeBidd(from: fancyBidder)
moneAuction.makeBidd(from: pureBidder)
