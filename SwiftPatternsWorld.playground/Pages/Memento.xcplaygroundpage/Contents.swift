
//MARK:-Memento
import Foundation
import UIKit

struct Point {
    var x: CGFloat
    var y: CGFloat
    var width: CGFloat
    var height: CGFloat
    var color: UIColor
    
    init(x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat) {
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.color = .black
    }
    
    init(x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat, color: UIColor) {
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.color = color
    }
}
protocol PainterDirector {
    func addPoint(_ point: Point)
    func printPoints()
    func save() -> PainterMemento
    func load(memento: PainterMemento)
}
class Painter: PainterDirector {
    
    private var points: [Point] = []
    
    func addPoint(_ point: Point) {
        points.append(point)
    }
    
    func printPoints() {
        points.forEach { (point) in
            print(point.x, point.y, point.color)
        }
    }
    
    func save() -> PainterMemento {
        return PainterMemento(points: points)
    }
    
    func load(memento: PainterMemento) {
        self.points = memento.points
    }
    
    fileprivate func clearView(_ view: UIView) {
        for subview in view.subviews {
            subview.removeFromSuperview()
        }
    }
}
class PainterMemento {
    
    var points: [Point] = []
    
    init(points: [Point]) {
        self.points = points
    }
}
class PainterCaretaker {
    
    public var painter: PainterDirector
    private var states: [PainterMemento] = []
    private var stateIndex: Int = 0
    
    init(painter: PainterDirector) {
        self.painter = painter
    }
    
    func show() {
        painter.printPoints()
    }
    //could save only 10 last memento for memory filters as option
    func save() {
        states.append(painter.save())
        resetStateIndex()
    }
    
    func load() {
        guard let lastState = states.last else { return }
        painter.load(memento: lastState)
        resetStateIndex()
    }
    
    func undo() {
        guard stateIndex > 0 else { return }
        stateIndex -= 1
        painter.load(memento: states[stateIndex])
    }
    
    func redu() {
        guard stateIndex < states.endIndex else { return }
        stateIndex += 1
        painter.load(memento: states[stateIndex])
    }
    
    fileprivate func resetStateIndex() {
        stateIndex = states.endIndex
    }
}
let point1 = Point(x: 1.0, y: 1.0, width: 3.0, height: 3.0, color: .blue)
let point2 = Point(x: 2.0, y: 2.0, width: 3.0, height: 3.0, color: .orange)
let point3 = Point(x: 3.0, y: 3.0, width: 3.0, height: 3.0, color: .red)
let point4 = Point(x: 4.0, y: 4.0, width: 3.0, height: 3.0, color: .green)

let painter = Painter()
painter.addPoint(point1)
painter.addPoint(point2)
let careTaker = PainterCaretaker(painter: painter)
careTaker.save()
painter.addPoint(point3)
careTaker.save()
painter.addPoint(point4)
careTaker.save()
careTaker.load()
careTaker.show()


