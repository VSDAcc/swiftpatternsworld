
import Foundation

//MARK:-Visitor
struct Client {
    var name: String
    var age: Int
}
protocol Taxi {
    func accept(visitor: TaxiVisitor)
}
class Uber: Taxi {
    
    var taxiName: String
    var taxiRent: Int = 80
    var discount: Int = 20
    
    init(name: String) {
        self.taxiName = name
    }
    
    func accept(visitor: TaxiVisitor) {
        visitor.visit(taxi: self)
    }
}
class Uclon: Taxi {
    
    var taxiName: String
    var taxiRent: Int = 100
    
    init(name: String) {
        self.taxiName = name
    }
    
    func accept(visitor: TaxiVisitor) {
        visitor.visit(taxi: self)
    }
}
protocol VisitorsDirectorProtocol {
    var clients: [Client] { get set }
    func appendClient(_ client: Client)
}
class VisitorsDirector: VisitorsDirectorProtocol {
    
    var clients: [Client] = []
    
    func appendClient(_ client: Client) {
        clients.append(client)
    }
}
protocol TaxiVisitor {
    func visit(taxi: Uber)
    func visit(taxi: Uclon)
}
class ScandinavianVisitor: TaxiVisitor {
    
    var director: VisitorsDirectorProtocol
    var bill: Int = 0
    
    init(director: VisitorsDirectorProtocol) {
        self.director = director
    }
    
    func visit(taxi: Uber) {
        bill = taxi.taxiRent - taxi.discount
        director.clients.forEach { (client) in
            print("\(client.name) recieved bill \(bill) in taxi \(taxi.taxiName)")
        }
    }
    
    func visit(taxi: Uclon) {
        bill = taxi.taxiRent
        director.clients.forEach { (client) in
            print("\(client.name) recieved bill \(bill) in taxi \(taxi.taxiName)")
        }
    }
    
    func generateBill(taxis: [Taxi]) {
        taxis.forEach { (taxi) in
            taxi.accept(visitor: self)
        }
    }
}
let uclon = Uclon(name: "Uclon")
let uber = Uber(name: "Uber")
let director = VisitorsDirector()
director.appendClient(Client(name: "Yosh", age: 21))
director.appendClient(Client(name: "Bob", age: 21))
let scandinavianVisitor = ScandinavianVisitor(director: director)
scandinavianVisitor.generateBill(taxis: [uclon, uber])


